from django.conf.urls import url, include

#from rest_framework import views, serializers, status
#from rest_framework.response import Response

from django.views import generic
from .views import signupView, createTopicView, fetchTopicsView, fetchCommentView
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from django.views.decorators.csrf import csrf_exempt

#from django.conf import settings
#from django.conf.urls.static import static

urlpatterns = [ 
    url(r'^$', get_schema_view()),
    url(r'^auth/', include(
        'rest_framework.urls', namespace='rest_framework')),
    url(r'^auth/token/obtain/$', TokenObtainPairView.as_view()),
    url(r'^auth/token/refresh/$', TokenRefreshView.as_view()),
    url(r'^signup/$', signupView.as_view()),
    url(r'^topic/create/$', createTopicView.as_view()),
    #url(r'^comment/post/$', addListingView.as_view()),
    url(r'^topics/list/$', fetchTopicsView.as_view()),
    url(r'^comment/list/(?P<id>\d+/)$', fetchCommentView.as_view()),
]
