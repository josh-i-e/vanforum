from django.shortcuts import render
import json
from rest_framework.permissions import IsAuthenticatedOrReadOnly, AllowAny
from django.contrib.auth.models import User
from api.serializers import signUpSerializer
from rest_framework import views, status
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
#from django.core import serializers
from .serializers import createTopicSerializer
from .models import Topics, Comments


# Create your views here.


class signupView(views.APIView):
	permission_classes = (AllowAny,)
	def post(self, request, *args, **kwargs):
		data = request.data.copy()
		serializer = signUpSerializer(data=data)
		serializer.is_valid(raise_exception=True)
		try:
			user = User.objects.create_user(**serializer.data)
			response = {
				'code': '00',
				'status': 'success'
			} 
			return Response(
				response, status=status.HTTP_201_CREATED)
		except:
			response = {
				'code': '01',
				'status': 'failed',
				'errors': serializer.errors
			}     
			return Response(
				response, status=status.HTTP_400_BAD_REQUEST)



class createTopicView(views.APIView):
	def post(self, request, *args, **kwargs):
		print (request)
		data = request.data.copy()
		serializer = createTopicSerializer(data=data)
		serializer.is_valid(raise_exception=True)
		try:
			serializer.save(author=request.user)
			response = {
				'code': '00',
				'status': 'success'
			} 
			return Response(
				response, status=status.HTTP_201_CREATED)
		except:
			response = {
				'code': '01',
				'status': 'failed'
			}     
			return Response(
				response, status=status.HTTP_400_BAD_REQUEST)


class fetchTopicsView(views.APIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	def get(self, request, *args, **kwargs):
		topics = Topics.objects.all().order_by('-pub_date')
		data = [
			{
				'id': topic.id,
				'author': topic.author.username, 
				'title': topic.title,
				'text': topic.text,
				'category': topic.category,
				'date': topic.pub_date
			} for topic in topics
		]
		
		return Response(
			data, status=status.HTTP_201_CREATED)
		



class fetchCommentView(views.APIView):
	permission_classes = (IsAuthenticatedOrReadOnly,)
	def get(self, request, id, *args, **kwargs):
		#topics = Topics.objects.all().order_by('-pub_date')
		comments = Comments.objects.all().filter(topic_id=id).order_by('-pub_date')
		data = [
			{
				'id': comments.id,
				'author': comments.author.username, 
				'title': comments.topic.title,
				'text': comments.topic.text,
				'category': comments.topic.category,
				'date': comments.pub_date
			} for topic in topics
		]
		
		return Response(
			data, status=status.HTTP_201_CREATED)
		