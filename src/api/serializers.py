from rest_framework import serializers
from .models import Topics
from django.contrib.auth.models import User

class createTopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topics
        exclude = ('pub_date', 'author',)



class signUpSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')

