import jwtDecode from 'jwt-decode'
import * as fetchThreads from '../actions/fetchThreads'
const initialState = {}
export default (state=initialState, action) => {
	switch(action.type) {
		case fetchThreads.FETCHTHREADS_SUCCESS:
		console.log(action.payload)
			return {
				...state,
				...action.payload	
			}

		case fetchThreads.FETCHTHREADS_FAILURE:
			return {
				...state,
				errors: 
					action.payload.response || 
						{'non_field_errors': action.payload.statusText}
			}
		default:
			return state
		}
}

export function errors(state) {
	return state.fetchThreads.errors
}
export function accessThreads(state) {
    if (state.fetchThreads) {
        return state.fetchThreads
    }
}