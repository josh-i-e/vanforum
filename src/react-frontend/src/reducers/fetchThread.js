import jwtDecode from 'jwt-decode'
import * as fetchThread from '../actions/fetchThread'
const initialState = {}
export default (state=initialState, action) => {
	switch(action.type) {
		case fetchThread.FETCHTHREAD_SUCCESS:
		
			return {
				...state,
				...action.payload	
			}

		case fetchThread.FETCHTHREAD_FAILURE:
			return {
				...state,
				errors: 
					action.payload.response || 
						{'non_field_errors': action.payload.statusText}
			}
		default:
			return state
		}
}

export function errors(state) {
	return state.fetchThread.errors
}
export function accessThread(state) {
    if (state.fetchThread) {
        return state.fetchThread
    }
}