import jwtDecode from 'jwt-decode'
import * as postTopic from '../actions/postTopic'
const initialState = {
	errors: {},
	message: null
}
export default (state=initialState, action) => {
	switch(action.type) {
		case postTopic.POSTTOPIC_SUCCESS:
			return {
				...state,
				errors: {},
				message: "Posting successful. You will be redirected in 3 seconds."			
		}

		case postTopic.POSTTOPIC_FAILURE:
			return {
				...state,
				errors: 
					action.payload.response || 
						{'non_field_errors': action.payload.statusText}
			}
		default:
			return state
		}
}

export function errors(state) {
	return state.postTopic.errors
}
export function message(state) {
	return state.postTopic.message
}