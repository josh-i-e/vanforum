import { withAuth } from '../reducers'
import { RSAA } from 'redux-api-middleware';

export const FETCHTHREADS_REQUEST = '@@fetchThreads/FETCHTHREADS_REQUEST';
export const FETCHTHREADS_SUCCESS = '@@fetchThreads/FETCHTHREADS_SUCCESS';
export const FETCHTHREADS_FAILURE = '@@fetchThreads/FETCHTHREADS_FAILURE';

export const fetchThreads = (data) => ({
  [RSAA]: {
    endpoint: '/api/topics/list/',
    method: 'GET',
    types: [
      FETCHTHREADS_REQUEST, FETCHTHREADS_SUCCESS, FETCHTHREADS_FAILURE
    ]
  }
})
