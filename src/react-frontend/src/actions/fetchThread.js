import { withAuth } from '../reducers'
import { RSAA } from 'redux-api-middleware';

export const FETCHTHREAD_REQUEST = '@@fetchThread/FETCHTHREAD_REQUEST';
export const FETCHTHREAD_SUCCESS = '@@fetchThread/FETCHTHREAD_SUCCESS';
export const FETCHTHREAD_FAILURE = '@@fetchThread/FETCHTHREAD_FAILURE';

export const fetchThread = (data) => ({
  [RSAA]: {
    endpoint: 'comment/list/'+data,
    method: 'GET',
    types: [
      FETCHTHREAD_REQUEST, FETCHTHREAD_SUCCESS, FETCHTHREAD_FAILURE
    ]
  }
})
