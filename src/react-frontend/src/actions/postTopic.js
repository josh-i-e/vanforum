import { withAuth } from '../reducers'
import { RSAA } from 'redux-api-middleware';

export const POSTTOPIC_REQUEST = '@@postTopic/POSTTOPIC_REQUEST';
export const POSTTOPIC_SUCCESS = '@@postTopic/POSTTOPIC_SUCCESS';
export const POSTTOPIC_FAILURE = '@@postTopic/POSTTOPIC_FAILURE';

export const postTopic = (data) => ({
  [RSAA]: {
    endpoint: '/api/topic/create/',
    method: 'POST',
    body: JSON.stringify(data),
    headers: withAuth({ 'Content-Type': 'application/json' }),
    types: [
      POSTTOPIC_REQUEST, POSTTOPIC_SUCCESS, POSTTOPIC_FAILURE
    ]
  }
})
