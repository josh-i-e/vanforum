import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, StaticRouter, // for server rendering    
    Route,
    Link
} from 'react-router-dom'
import { Provider } from "react-redux";
import configureStore from "./store";
import './index.css';
import App from './components/App';
import Login from './components/Login';
import TopicForm from './components/TopicCreate';
import TopicView from './components/TopicView';
import registerServiceWorker from './registerServiceWorker';
import createHistory from 'history/createBrowserHistory'
import { PersistGate } from 'redux-persist/integration/react'
const history = createHistory()
const store = configureStore(history);

ReactDOM.render(
    <Provider store={store}>
       
        <Router>
            <Switch>
                <Route path="/" exact component={App}/>
                <Route path="/login" component={Login}/>
                <Route path="/topic/create" component={TopicForm}/>
                
                <Route path="/topic/id/:id" component={TopicView}/>
            </Switch>
        </Router>
        
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
