import LoginForm from './presenter'

import { connect } from 'react-redux'
import { Redirect } from 'react-router'


import {login} from  '../../actions/auth'
import {authErrors, isAuthenticated} from '../../reducers'

const mapStateToProps = (state) => ({
	errors: authErrors(state),
	isAuthenticated: isAuthenticated(state)
})
const mapDispatchToProps = (dispatch) => ({
	onSubmit: (username, password) => {
		dispatch(login(username, password))
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
