import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import './Login.css';
import { Modal, Radio } from 'antd';
import { List, Avatar, Spin } from 'antd';
import { browserHistory } from 'react-router';
import { Redirect } from 'react-router';


const { TextArea } = Input;
const FormItem = Form.Item;




class TopicView extends React.Component {

  constructor(props) {
    super(props);  
    this.state = {
      loading: true,
      loadingMore: false,
      showLoadingMore: true,
      result: [],
      threads : {},      
    };
  }

  componentDidMount() {
    this.props.fetchThread();      
  }

  componentWillReceiveProps(props) {    
    this.setState({
      thread : props.thread,
      result: this.objectToList(props.thread),
    })    
  };

  objectToList(obj) {
    let result = []		
    for (let x in obj) {			
      if( obj.hasOwnProperty(x) ) {
        result.push(obj[x]);
      } 
    } 
    return result;
  }


  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <div>
      <h3 style={{ marginBottom: 16 }}>Default Size</h3>
      <List
      header={<div>Header</div>}
      footer={<div>Footer</div>}
      bordered
      dataSource={this.state.result}
      renderItem={item => (<List.Item>{item.text}</List.Item>)}
      />
      </div>
    );
  }
}

export default TopicView;