import TopicView from './presenter'

import { connect } from 'react-redux'
import { Redirect } from 'react-router'


import {fetchThread} from  '../../actions/fetchThread'
import {accessThread} from '../../reducers/fetchThread'

const mapStateToProps = (state) => ({
	thread : accessThread(state)
})


const mapDispatchToProps = (dispatch) => ({
	fetchThread: (data) => {
		dispatch(fetchThread(data))
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(TopicView);
