import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import './Login.css';

import { Modal, Radio } from 'antd';
const { TextArea } = Input;

const FormItem = Form.Item;


const AppTopicForm = Form.create()(
  (props) => {
    const { visible, onCancel, onCreate, form, onInputChange } = props;
    const { getFieldDecorator } = form;
    return (
      <Modal
        visible={visible}
        title="Create a new topic"
        okText="Create"
        onCancel={onCancel}
        onOk={onCreate}
      >
        <Form layout="vertical">
          <FormItem label="Title">
            {getFieldDecorator('title', {
              rules: [{ required: true, message: 'Please input the topic!' }],
            })(
              <Input name="title" onChange={onInputChange}/>
            )}
          </FormItem>

          <FormItem label="Category">
            {getFieldDecorator('category', {
              rules: [{ required: true, message: 'Please input the category!' }],
            })(
              <Input name="category" onChange={onInputChange}/>
            )}
          </FormItem>
          
          <FormItem label="Message">
            {getFieldDecorator('text', {
              rules: [{ required: true, message: 'Please input the topic!' }],})(<TextArea name="text" rows={4} onChange={onInputChange}/>
            )}
          </FormItem>
          
        </Form>
      </Modal>
    );
  }
);



class AppTopicPage extends React.Component {
  state = {
    visible: true,
    data: {},
  };
  showModal = () => {
    this.setState({ visible: true });
  }
  handleCancel = () => {
    this.setState({ visible: false });
  }
  handleInputChange = (event) => {
    const target = event.target,
    value = target.type === 
        'checkbox' ? target.checked : target.value,
    name = target.name
    
    this.setState({
        data:{...this.state.data, [name]: value}
    });
    
}
  handleCreate = () => {
    const form = this.form;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }

      form.resetFields();
      this.props.onSubmit(this.state.data);
      this.setState({ visible: false });
    });
  }
  saveFormRef = (form) => {
    this.form = form;
  }
  render() {
    return (
      <div>
        <Button type="primary" onClick={this.showModal}>New Topic</Button>
        <AppTopicForm
          ref={this.saveFormRef}
          visible={this.state.visible}
          onCancel={this.handleCancel}
          onCreate={this.handleCreate}
          onInputChange={this.handleInputChange}
        />
      </div>
    );
  }
}
export default AppTopicPage;