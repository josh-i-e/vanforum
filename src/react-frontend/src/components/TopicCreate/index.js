import AppTopicPage from './presenter'

import { connect } from 'react-redux'
import { Redirect } from 'react-router'


import {postTopic} from  '../../actions/postTopic'
import {authErrors, isAuthenticated} from '../../reducers'

const mapStateToProps = (state) => ({
	errors: authErrors(state),
	isAuthenticated: isAuthenticated(state)
})
const mapDispatchToProps = (dispatch) => ({
	onSubmit: (data) => {
		dispatch(postTopic(data))
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(AppTopicPage);
