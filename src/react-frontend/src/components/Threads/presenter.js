import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import './Login.css';
import { Modal, Radio } from 'antd';
import { List, Avatar, Spin } from 'antd';
import { browserHistory } from 'react-router';
import { Redirect } from 'react-router';


const { TextArea } = Input;
const FormItem = Form.Item;





const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);

class Threads extends React.Component {

  constructor(props) {
    super(props);  
    this.state = {
      loading: true,
      loadingMore: false,
      showLoadingMore: true,
      result: [],
      threads : {},      
    };
  }

  componentDidMount() {
    this.props.fetchThreads();      
  }

  componentWillReceiveProps(props) {    
    this.setState({
      threads : props.threads,
      result: this.objectToList(props.threads),
      loading: false,
    })    
  };
  onLoadMore = () => {
    this.setState({
      loadingMore: true,
    });
    this.props.fetchThreads();//pass a counter here
    this.setState({
        loadingMore: false,
    }, () => {
      // Resetting window's offsetTop so as to display react-virtualized demo underfloor.
      // In real scene, you can using public method of react-virtualized:
      // https://stackoverflow.com/questions/46700726/how-to-use-public-method-updateposition-of-react-virtualized
      window.dispatchEvent(new Event('resize'));
      });
  }

  objectToList(obj) {
    let result = []		
    for (let x in obj) {			
      if( obj.hasOwnProperty(x) ) {
        result.push(obj[x]);
      } 
    } 
    return result;
  }
  topicClick = (id) => {
		//event.preventDefault()
    //history.push('/topic/id/'+id+'/')
    <Redirect to='/login'/>
    
	}

  render() {
    const { loading, loadingMore, showLoadingMore, data } = this.state;
    const loadMore = showLoadingMore ? (
      <div style={{ textAlign: 'center', marginTop: 12, height: 32, lineHeight: '32px' }}>
        {loadingMore && <Spin />}
        {!loadingMore && <Button onClick={this.onLoadMore}>loading more</Button>}
      </div>
    ) : null;
    return (
      <List
        className="demo-loadmore-list"
        loading={loading}
        itemLayout="horizontal"
        loadMore={loadMore}
        dataSource={this.state.result}
        renderItem={item => (
          <List.Item actions={[<IconText type="like-o" text="156" />, <IconText type="message" text="2" />]}>
            <List.Item.Meta
              avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
              title={item.author}
              description={<a  onClick={this.topicClick(item.id)}>{item.title}</a>}
            />
            <div>{item.category}</div>
          </List.Item>
        )}
      />
    );
  }
}

export default Threads;