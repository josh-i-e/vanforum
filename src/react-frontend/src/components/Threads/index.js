import Threads from './presenter'

import { connect } from 'react-redux'
import { Redirect } from 'react-router'


import {fetchThreads} from  '../../actions/fetchThreads'
import {accessThreads} from '../../reducers/fetchThreads'

const mapStateToProps = (state) => ({
	threads : accessThreads(state)
})


const mapDispatchToProps = (dispatch) => ({
	fetchThreads: (data) => {
		dispatch(fetchThreads(data))
	}
})

export default connect(mapStateToProps, mapDispatchToProps)(Threads);
