import React, { Component } from 'react';
//import logo from './logo.svg';
//import './App.css';
import { Affix, Button } from 'antd';
import { Row, Col } from 'antd';


import { Layout, Menu, Breadcrumb, Icon } from 'antd';
const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;


class App extends Component {
  render() {
    return (
      
        <Layout>
        <Header style={{ position: 'fixed', width: '100%', lineHeight: '64px', background: '#2196F3' }}>
          <Row>
            <Col span={6} style={{ fontsize: 32, color: '#fff' }}>VanForum</Col>
            <Col span={2} offset={14}>
              <Icon type="search" style={{ color: '#fff' }} />  
            </Col>
            <Col span={2} >
              <Icon type="poweroff" style={{ color: '#fff' }} />                
            </Col>
          </Row>
        </Header>
        <Layout>
            
        </Layout>
        </Layout>
      
    );
  }
}

export default App;

