import { Layout } from 'antd';
const { Header, Footer, Sider, Content } = Layout;


class Content extends Component {
    render() {
      return (
        
        <Layout>
            <Content>Content</Content>
            <Sider>Sider</Sider>
        </Layout>
        
      );
    }
}
  
export default Content;
