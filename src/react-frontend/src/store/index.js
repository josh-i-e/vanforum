import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/index";
import { apiMiddleware } from 'redux-api-middleware';
import { routerMiddleware } from 'react-router-redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import logger from 'redux-logger'


export default function configureStore(history) {
    const persistConfig = {
        key: 'root',
        storage: storage,
        whitelist: ['auth'],
        stateReconciler: autoMergeLevel2 
    };
       
    const Reducer = persistReducer(persistConfig, rootReducer);

    const store = createStore(
        Reducer, {},
        applyMiddleware(
          apiMiddleware, 
          routerMiddleware(history),
          logger,)
    )
    const persistor = persistStore(store)
  return store
}

